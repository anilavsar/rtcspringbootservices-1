package com.dci.rest.service.library;


import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;

import com.dci.rest.bdo.ComponentBDO;
import com.dci.rest.bdo.TableBDO;
import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.ContextBean;
import com.dci.rest.model.ContextSchema;
import com.dci.rest.model.ResloveVarBean;
import com.dci.rest.model.Response;
import com.dci.rest.model.TableComponent;
import com.fasterxml.jackson.core.JsonProcessingException;

@Service
public class ComponentService {
	
	@Autowired ComponentBDO componentBDO;
	//@Autowired TableBDO tableBDO;
	
	public Response<Object> isValidName(String name,HttpServletRequest request) throws Exception {
		return componentBDO.isValidName(name,request);
	}
	
	public Response<Object> createComponent(ComponentBean component, Map<String,Object> reqParams,BindingResult bindingResult,HttpServletRequest request) throws Exception {
		return componentBDO.createComponent(component, reqParams,bindingResult,request);
	}
	public Response<Object> editComponent(String idToEdit, ComponentBean component, Map<String,Object> reqParams,HttpServletRequest request) throws Exception {
		ComponentBean componentTypeObj = new ComponentBean();
		component.setElementInstanceId(idToEdit);
		
		componentTypeObj = componentBDO.checkComponentType(component);
		/*if (!CheckString.isValidString(componentTypeObj.getType())) {*/
			if (componentTypeObj.getType().equals("table")) {
				return componentBDO.editTableComponent(idToEdit, component, reqParams,request);
			/*}*/
		}
		return componentBDO.editComponent(idToEdit, component, reqParams,request);
	}

	public Response<Object> getComponent(String componenttId, Map<String,Object> reqParams,HttpServletRequest request) throws Exception {
		return componentBDO.getComponent(componenttId, reqParams,request);
	}
	public Response<Object> getContextList(HttpServletRequest request) throws Exception {
		return componentBDO.getContextList(request);
	}
	public Response<Object> getLocalList(HttpServletRequest request) throws Exception {
		return componentBDO.getLocalList(request);
	}
	public Response<Object> getAssociationData(HttpServletRequest request) throws Exception {
		return componentBDO.getAssociationData(request);
	}
	public Response<Object> getFundList(HttpServletRequest request)throws Exception {
		return componentBDO.getFundList(request);
	}
	public Response<Object> getDocumentList(HttpServletRequest request)throws Exception {
		return componentBDO.getDocumentList(request);
	}
	public Response<Object> getDocumentTypeList(HttpServletRequest request)throws Exception {
		return componentBDO.getDocTypeList(request);
	}
	public Response<Object> getCreatedUpdatedBy(HttpServletRequest request)throws Exception {
		return componentBDO.getCreatedUpdatedBy(request);
	}
	public Response<Object> getStatusList(HttpServletRequest request)throws Exception {
		return componentBDO.getStatusList(request);
	}
	public Response<Object> getAssetList(HttpServletRequest request)throws Exception {
		return componentBDO.getAssetClassList(request);
	}
	public Response<Object> setComponentAssociation(String componentId,ComponentBean component,HttpServletRequest request)throws Exception {
		return componentBDO.setComponentAssociation(componentId, component,request);
	}
	public Response<Object>  getComponentAssociation(String componentId,HttpServletRequest request)throws Exception {
		return componentBDO.getComponentAssociation(componentId, request);
	}
	public Response<Object> getComponentComments(String componentId, HttpServletRequest request) throws Exception {
		return componentBDO.getComponentComments(componentId, request);
	}
	public Response<Object> createComponentComment(String componentId,ComponentBean component, HttpServletRequest request,BindingResult result) throws Exception {
		return componentBDO.createComponentComment(componentId,component, request,result);
	}
	public Response<Object> getLinkageComponent(String componentId,HttpServletRequest request) throws Exception {
		return componentBDO.getLinkageComponent(componentId, request);
	}
	public Response<Object> processComponentPrimaryFlag(String componentId,String flag,HttpServletRequest request) throws Exception {
		return componentBDO.processComponentPrimaryFlag(componentId,flag, request);
	}
	public Response<Object> getComponentHistoryData(String componentId, HttpServletRequest request) throws Exception {
		return componentBDO.getComponentHistoryData(componentId, request);
	}
	public Response<Object> linkComponents(String componentId,@RequestBody ComponentBean component,HttpServletRequest request) throws Exception {
		return componentBDO.linkComponents(componentId,component, request);
	}
	public Response<Object> disassociateSecondaryComonent(String primaryComponentId,ComponentBean component,HttpServletRequest request) throws Exception {
		return componentBDO.disassociateSecondaryComonent(primaryComponentId,component, request);
	}
	public Response<Object> getComponentPrivilege(HttpServletRequest request) throws Exception {
		return componentBDO.getComponentPrivilege(request);
	}
	public Response<Object> deactivateComponent(ComponentBean component,BindingResult bindingresult,HttpServletRequest request) throws Exception {
		return componentBDO.deactivateComponent(component,bindingresult,request);
	}	
	public Response<Object> getComponentBlackline(Map<String,String> reqParams,HttpServletRequest request) throws Exception {
		return componentBDO.getComponentBlackline(reqParams,request);
	}
	public Response<Object> getCreateComponentBlackline(Map<String,String> reqParams,HttpServletRequest request) throws Exception {
		return componentBDO.getCreateComponentBlackline(reqParams,request);
	}
	public Response<Object> getVersionBlackline(Map<String,String> reqParams,HttpServletRequest request) throws Exception {
		return componentBDO.getVersionBlackline(reqParams,request);
	}
	public Response<Object> getVariables(Map<String, String> reqParams,HttpServletRequest request) {
		return componentBDO.getVariables(reqParams,request);
	}
	public Response<Object> getImages(Map<String, String> reqParams,HttpServletRequest request) {
		return componentBDO.getImages(reqParams,request);
	}
	public Response<Object> getInlineStyles(Map<String, String> reqParams,HttpServletRequest request) {
		return componentBDO.getInlineStyles(reqParams,request);
	}
	public Response<Object> addToFavoriteLibrary(String selectedComponents ,HttpServletRequest request){
		return componentBDO.addToFavoriteLibrary(selectedComponents, request);
	}
	public Response<Object> getLibraryFavorite(HttpServletRequest request) throws JsonProcessingException {
		return componentBDO.getLibraryFavorite(request);
	}
	public Response<Object> removeFromFavoriteLibrary(String selectedComponents ,HttpServletRequest request) throws JsonProcessingException {
		return componentBDO.removeFromFavoriteLibrary(selectedComponents, request);
	}
	public Response<Object> getComponentTags( Map<String, String>  reqParams,HttpServletRequest request){
		return componentBDO.getComponentTags(reqParams,request);
	}
	public Response<Object> createTags(Map<String, String>  reqParams,ComponentBean component,HttpServletRequest request){
		return componentBDO.createTags(reqParams,component,request);
	}
	public Response<Object> setComponentTags(ComponentBean component,BindingResult bindingResult,Map<String,String> reqParams,HttpServletRequest request){
		return componentBDO.setComponentTags(component,bindingResult,reqParams,request);
	}
	public Response<Object> createLinkageComponent(ComponentBean component,BindingResult bindingResult,Map<String,String> reqParams,HttpServletRequest request){
		return componentBDO.createLinkageComponent(component,bindingResult,reqParams,request);
	}

	public Response<Object> getTableTypes(Map<String, String> pathParam,HttpServletRequest request) {
		return componentBDO.getTableTypes(pathParam,request);
	}
	public Response<Object> getElementStylesList(HttpServletRequest request,String type) {
		return componentBDO.getElementStylesList(request,type);
	}
	public Response<Object> getCompType(HttpServletRequest request) {
		return componentBDO.getComponentTypes(request);
	}

	public Response<Object> createTableComponent(TableComponent table,BindingResult bindingResult,HttpServletRequest request) {		
		return componentBDO.createTableComponent(table,bindingResult,request);
	}

	public Response<Object> editTableComponent(String string, TableComponent table,Map<String,Object> pathVariablesMap,HttpServletRequest request) {
		return componentBDO.editTableComponent(string, table, pathVariablesMap,request);
	}
	public Response<Object> updateSecondaryCompStatus(String componentId,ComponentBean component, HttpServletRequest request,BindingResult bindingResult) {
		return componentBDO.updateSecondaryCompStatus(componentId,component, request,bindingResult);
	}
	public Response<Object> createContext(ContextBean contextBean, HttpServletRequest request,BindingResult bindingResult) {
		return componentBDO.createContext(contextBean, request,bindingResult);
	}
	
	public Response<Object> editContext(String contextId , ContextBean contextBean,BindingResult bindingResult,Map<String,Object> reqParamst,HttpServletRequest request) {
		return componentBDO.editContext(contextId, contextBean,bindingResult, reqParamst,request);
	}
	
	public Response<Object> getFootnoteList(Map<String,Object> reqParamst,HttpServletRequest request) {
		return componentBDO.getFootnoteList(reqParamst,request);
	}
	public Response<Object> fundListonAssetClass(Map<String,Object> reqParamst,ComponentBean componentBean,HttpServletRequest request) {
		return componentBDO.fundListonAssetClass(reqParamst,componentBean,request);
	}
	public Response<Object> getComponentStatusList(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		return componentBDO.getComponentStatusList(pathVariablesMap,request);
	}
	public Response<Object> getAuthUsers(HttpServletRequest request) {
		return componentBDO.getAuthUsers(request);
	}
	public Response<Object> bulkdataUpdate(HttpServletRequest request,ComponentBean component) {
		return componentBDO.bulkdataUpdate(request,component);
	}
	public Response<Object> getAffectedComponent(String fimportstatusid,HttpServletRequest request) {
		return componentBDO.getAffectedComponent(fimportstatusid,request);
	}
	public Response<Object> revertToVersion(ComponentBean component,HttpServletRequest request,BindingResult bindingResult) {
		return componentBDO.revertToVersion(component,request,bindingResult);
	}
	public Response<Object> createPrimarySecondaryCatogory(Map<String, String> pathVariablesMap,ContextBean contextBean,HttpServletRequest request,BindingResult bindingResult) {
		return componentBDO.createPrimarySecondaryCatogory(pathVariablesMap,contextBean,request,bindingResult);
	}
	public Response<Object> removeCategory(ContextBean contextBean,HttpServletRequest request,BindingResult bindingResult) {
		return componentBDO.removeCategory(contextBean,request,bindingResult);
	}
	public Response<Object> editSubContentCatogory(Map<String, String> pathVariablesMap,ContextBean contextBean,HttpServletRequest request,BindingResult bindingResult) {
		return componentBDO.editSubContentCategory(pathVariablesMap,contextBean,request,bindingResult);
	}
	public Response<Object> orderContentCatogory(ContextBean contextBean,HttpServletRequest request,BindingResult bindingResult) {
		return componentBDO.orderContentCatogory(contextBean,request,bindingResult);
	}
	public Response<Object> addPermissionToContentCategory(ContextBean contextBean,HttpServletRequest request,BindingResult bindingResult) {
		return componentBDO.addPermissionToContentCategory(contextBean,request,bindingResult);
	}
	public Response<Object> authEditSubContentCatogory(ContextBean contextBean,HttpServletRequest request,BindingResult bindingResult) {
		return componentBDO.authEditSubContentCatogory(contextBean,request,bindingResult);
	}
	public Response<Object> editContentCatogory(ContextBean contextBean,HttpServletRequest request,BindingResult bindingResult) {
		return componentBDO.editContentCategory(contextBean,request,bindingResult);
	}
	public Response<Object> getContentCategory(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		return componentBDO.getContentCategory(pathVariablesMap,request);
	}
	public Response<Object> getContentCatByGroup(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		return componentBDO.getContentCatByGroup(pathVariablesMap,request);
	}
	
	public Response<Object> availableToPrimary(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		return componentBDO.availableToPrimary(pathVariablesMap,request);
	}

	public Response<Object> createCategorySchema(ContextSchema contextSchema, HttpServletRequest request,
			BindingResult bindingResult) {
		return componentBDO.createCategorySchema(contextSchema,request);
	}
	public Response<Object> getShareClassByFund(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		return componentBDO.getShareClassByFund(pathVariablesMap,request);
	}
	public Response<Object> resolveComponentVariables(Map<String, String> pathVariablesMap,ResloveVarBean resloveVarBean ,HttpServletRequest request) {
		return componentBDO.resolveComponentVariables(pathVariablesMap,resloveVarBean,request);
	}
	public Response<Object> previewVariableValue(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		return componentBDO.previewVariableValue(pathVariablesMap,request);
	}
	public Response<Object> getAllComponentsForCart(Map<String, String> pathVariablesMap,HttpServletRequest request) {
		return componentBDO.getAllComponentsForCart(pathVariablesMap,request);
	}
	public Response<Object> saveAllComponentsToCart(String user, String client,ComponentBean componentBean ,HttpServletRequest request) {
		return componentBDO.saveAllComponentsToCart(user,client,componentBean,request);
	}

	public Response<Object> removeComponentsFromCart(String user, String client,ComponentBean componentBean,HttpServletRequest request) {
		return componentBDO.removeComponentsFromCart(user,client,componentBean,request);
	}
	
}
