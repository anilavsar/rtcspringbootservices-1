package com.dci.rest.controller.library;


import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dci.rest.auth.service.AuthService;
import com.dci.rest.model.APIModel;
import com.dci.rest.model.Response;

@RestController
@RequestMapping({"${API}"})
@CrossOrigin
public class APIExplorerController {
	
	private Logger developerLog = Logger.getLogger(APIExplorerController.class);


	@Autowired
	AuthService authService; 

	@RequestMapping(value = {"${GET_ALL_API}","${GET_API_DETAILS}"},method = RequestMethod.GET)
	public Response<Object> getApiList(@PathVariable Map<String, String> pathVariablesMap,HttpServletRequest request) throws SQLException {	
		MDC.put("category","com.dci.rest.controller.library");
		developerLog.warn("Entering into com.dci.rest.controller.library || Method Name : userAuthServerDetails() ||");
		APIModel aPIBean = new APIModel();
		Response<Object> responseVO = new Response<Object>();
		responseVO = authService.getApiList(pathVariablesMap,request);
		return responseVO;
	}
	@RequestMapping(value = "${API_REGISTRATION}",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> registerRestUser(HttpServletRequest request,@RequestBody APIModel aPIBean) {	
		MDC.put("category","com.dci.rest.controller.welcome.");
		developerLog.warn("Entering into com.dci.rest.controller.library.RegistrationController || Method Name : registerRestUser() ||");
		Response<Object> responseVO = new Response<Object>();
		responseVO  = authService.registerClient(request,aPIBean);
		return responseVO;
	}
	@RequestMapping(value = "${EDIT_API_REGISTRATION}",method = RequestMethod.PUT,produces = MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> updateregisterRestUser(HttpServletRequest request,@RequestBody APIModel aPIBean) {	
		MDC.put("category","com.dci.rest.controller.welcome.");
		developerLog.warn("Entering into com.dci.rest.controller.library.RegistrationController || Method Name : updateregisterRestUser() ||");
		Response<Object> responseVO = new Response<Object>();
		responseVO  = authService.updateregisterRestUser(request,aPIBean);
		return responseVO;
	}
	@RequestMapping(value = "${REMOVE_API}", method = RequestMethod.DELETE)
	public Response<Object> removeAPI(HttpServletRequest request,@PathVariable Map<String, String> pathVariablesMap){
		MDC.put("category","com.dci.rest.controller.library");
		developerLog.debug("Entering into com.dci.rest.controller.library.MediaController || Method Name : removeAPI() || input : ");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = authService.removeAPI(pathVariablesMap,request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.MediaController || Method Name : removeAPI() || response : "+responseVO);
		return responseVO;		
	}
/*	@RequestMapping(value = "${ADD_REQUEST_TOKEN}",method = RequestMethod.POST)
	public Response<Object> addRequestToken(@RequestBody APIBean aPIBean) {	
		MDC.put("category","com.dci.rest.controller.welcome.");
		developerLog.warn("Entering into com.dci.rest.controller.library.RegistrationController || Method Name : addRequestToken() ||");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = authService.addRequestToken(aPIBean);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return responseVO;
	}
	
	@RequestMapping(value = "${ADD_ACCESSS_TOKEN}",method = RequestMethod.POST)
	public Response<Object> addAccessToken(@RequestBody APIBean aPIBean) {	
		MDC.put("category","com.dci.rest.controller.welcome.");
		developerLog.warn("Entering into com.dci.rest.controller.library.RegistrationController || Method Name : addRequestToken() ||");
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = authService.addAccessToken(aPIBean);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return responseVO;
	}*/
}
