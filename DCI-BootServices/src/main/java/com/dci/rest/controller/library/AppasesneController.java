package com.dci.rest.controller.library;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.Response;
import com.dci.rest.service.library.ComponentSearchService;


@RestController
@RequestMapping({"${APPASENSE}"})
@CrossOrigin
public class AppasesneController extends ControllerPreprocess{
	
	private Logger developerLog = Logger.getLogger(AppasesneController.class);
	
	@Autowired ComponentSearchService searchService;
	
	@RequestMapping(value = "${APPASENSE_DUPLICATE_SEARCH}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> searchDuplicateComponent( @PathVariable Map<String, String> pathVariablesMap,@Validated(ComponentBean.ValidationSearchLinkage.class) @RequestBody ComponentBean component,BindingResult bindingresult, HttpServletRequest request){
		MDC.put("category", "com.dci.rest.controller.library.AppasesneController");
		developerLog.debug("Entering into com.dci.rest.controller.library.AppasesneController || Method Name : AppasesneController() || input : "+pathVariablesMap);
		Response<Object> responseVO = new Response<Object>();
		try {
			responseVO = searchService.searchduplicateComponent(component, bindingresult, request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		developerLog.debug("Exiting from com.dci.rest.controller.library.AppasesneController || Method Name : searchDuplicateComponent() || response : "+responseVO);

	return responseVO;		
}
	
	@RequestMapping(value = "${APPASENSE_DUPLICATE_BY_KEY}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public Response<Object> searchDuplicateComponentByKey( @PathVariable Map<String, String> pathVariablesMap,@Validated(ComponentBean.ValidationSearchLinkage.class) @RequestBody ComponentBean component,BindingResult bindingresult,HttpServletRequest request){
		MDC.put("category", "com.dci.rest.controller.library.AppasesneController");
		developerLog.debug("Entering into com.dci.rest.controller.library.AppasesneController || Method Name : searchDuplicateComponentByKey() || input :"+pathVariablesMap+","+component);
		Response<Object> responseVO = new Response<Object>();

	try {
		responseVO = searchService.searchDuplicateComponentByKey(pathVariablesMap.get("componentId").toString(),component,bindingresult,request);
	} catch (Exception e) {
		e.printStackTrace();
	}
	developerLog.debug("Exiting from com.dci.rest.controller.library.AppasesneController || Method Name : searchDuplicateComponentByKey() || response : "+responseVO);
	return responseVO;		
}
}
