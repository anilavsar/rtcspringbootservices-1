package com.dci.rest.bdo;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.apache.xerces.dom.DocumentImpl;
import org.json.XML;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.dci.rest.bean.TableDetails;
import com.dci.rest.common.ComponentCommons;
import com.dci.rest.dao.AppaSlidesDAO;
import com.dci.rest.dao.ComponentDAO;
import com.dci.rest.dao.elastic.ComponentElasticDAO;
import com.dci.rest.dao.elastic.ComponentElasticDBDAO;
import com.dci.rest.dao.elastic.ElasticServerKafkaDAO;
import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.AppaSlide;
import com.dci.rest.model.AppaSlideDualDetail;
import com.dci.rest.model.AssetClass;
import com.dci.rest.model.CommentsBean;
import com.dci.rest.model.ComponentBean;
import com.dci.rest.model.ComponentPermissionBean;
import com.dci.rest.model.ComponentStatus;
import com.dci.rest.model.ContextBean;
import com.dci.rest.model.ContextSchema;
import com.dci.rest.model.DocumentBean;
import com.dci.rest.model.DocumentType;
import com.dci.rest.model.Filter;
import com.dci.rest.model.FootNoteAttributes;
import com.dci.rest.model.Fund;
import com.dci.rest.model.Locale;
import com.dci.rest.model.Order;
import com.dci.rest.model.PendingWork;
import com.dci.rest.model.Placement;
import com.dci.rest.model.ResloveVarBean;
import com.dci.rest.model.Response;
import com.dci.rest.model.Schema;
import com.dci.rest.model.SearchCriteria;
import com.dci.rest.model.SelectBean;
import com.dci.rest.model.ShareClass;
import com.dci.rest.model.Status;
import com.dci.rest.model.Style;
import com.dci.rest.model.StyleBean;
import com.dci.rest.model.TableComponent;
import com.dci.rest.model.TableType;
import com.dci.rest.model.Tag;
import com.dci.rest.model.UserBean;
import com.dci.rest.model.VariableBean;
import com.dci.rest.model.WorkFLStatus;
import com.dci.rest.utils.CheckString;
import com.dci.rest.utils.DBXmlManager;
import com.dci.rest.utils.StringUtility;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import springfox.documentation.spring.web.json.Json;

@Component
@SuppressWarnings({ "rawtypes", "unchecked" })
public class AppaSlidesBDO extends ComponentCommons {
	
	@Autowired
	AppaSlidesDAO appaSlidesDAO;
	/*
	 * @Autowired ComponentElasticDAO searchDAO;
	 * 
	 * @Autowired ElasticServerKafkaDAO elasticServerKafkaDAO;
	 */
	@Value("${ADD_FAV_LIMIT}" ) private String favlimit;
	@Value("${TEXT}" ) private String text;
	@Value("${SUBHEAD}" ) private String subhead;
	@Value("${IMAGE}" ) private String image;
	@Value("${VARIABLE}" ) private String variable;
	@Value("${SYS_SCHEMA}" ) private String schema;

	
	@Autowired UserPrivilegeBDO userPrevilege;
	
	
	private Logger developerLog = Logger.getLogger(ComponentBDO.class);
	private static final String FOR_FUND_TYPE = "VEHICLEASSETCLASS";
	private static final int commentLength = 600;
	private static final int searchType = 1006;
	private static String keyContextId = null;
	private static  String topContextId = null;

	public Response<Object> saveAppaSlides(HttpServletRequest request, List<AppaSlide> requestBody) {

		List<AppaSlideDualDetail> isSaveSuccessful = null;
		
		try {
			
			developerLog.debug("Entering the appaSlidesBDO.saveAppaSlides || Method Name : saveAppaSlides() || input : ");
			developerLog.debug("Calling the appaSlidesDAO.saveAppaSlides || Method Name : saveAppaSlides() || input : ");

			isSaveSuccessful = appaSlidesDAO.saveAppaSlides(request, requestBody);
			
			if (isSaveSuccessful != null) {
				return response(200, "SUCCESS", "Valid Output Generated", isSaveSuccessful);
			} else {
				return response(409, "ERROR", "No Output Generated", isSaveSuccessful);
			}
		}
		catch(Exception ex) {
			new DocubuilderException("Exception in com.dci.rest.bdo.AppaSlidesBDO || saveAppaSlides()", ex);
			return response(500, "ERROR", internalServerError, isSaveSuccessful);
		}

	}

	public Response<Object> checkAppaSlides(HttpServletRequest request, List<AppaSlide> requestBody) {

		List<AppaSlideDualDetail> isChangePresent = null;
		
		try {
			
			developerLog.debug("Entering the appaSlidesBDO.checkAppaSlides || Method Name : checkAppaSlides()");
			developerLog.debug("Calling the appaSlidesDAO.checkAppaSlides || Method Name : checkAppaSlides()");

			isChangePresent = appaSlidesDAO.checkAppaSlides(request, requestBody);
			
			if (isChangePresent != null) {
				return response(200, "SUCCESS", "Valid Output Generated", isChangePresent);
			} else {
				return response(409, "ERROR", "No Output Generated", isChangePresent);
			}
		}
		catch(Exception ex) {
			new DocubuilderException("Exception in com.dci.rest.bdo.AppaSlidesBDO || checkAppaSlides()", ex);
			return response(500, "ERROR", internalServerError, isChangePresent);
		}
	}
	
}
