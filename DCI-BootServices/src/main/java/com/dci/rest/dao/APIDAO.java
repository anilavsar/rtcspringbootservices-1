package com.dci.rest.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.dci.rest.exception.DocubuilderException;
import com.dci.rest.model.APIModel;
import com.dci.rest.model.SearchCriteria;
import com.dci.rest.utils.CheckString;
import com.fasterxml.jackson.databind.ObjectMapper;


@Component
public class APIDAO extends DataAccessObject {
	
	private Logger developerLog = Logger.getLogger(APIDAO.class);
	
	
	public APIModel apiDetails(String userId, String clientId, String userKey) {
		developerLog.debug("Entering into com.dci.rest.dao.APIDAO || Method Name : apiDetails() ||");
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		ObjectMapper mapper = null ;
		APIModel apiModel = null;
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			System.out.println("{CALL SP8_GETAPPENDEDKEYDETAILS  (" + clientId + "," + userId + ",key,?)}");
			cs = con.prepareCall("{CALL SP8_GETAPPENDEDKEYDETAILS  (?,?,?,?)}");
			cs.setString(1, userId);
			cs.setInt(2, CheckString.getInt(clientId));
			cs.setString(3, userKey);
			cs.registerOutParameter(4, Types.VARCHAR);
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				mapper = new ObjectMapper();
				apiModel = new APIModel();
				apiModel.setSchemaSelector(rs.getString("FAPISCHEMAID"));
				apiModel.setDescription(rs.getString("fsearch_data"));
			//	apiModel.setCriteria(mapper.readValue(rs.getString("fsearch_data"), SearchCriteria.class));
			//.setContext(rs.getString("FELEMENTCONTEXTID"));
			//	apiDetailList.add(apiModel);
			}
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.ComponentDAO || apiDetails()", e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.APIDAO || Method Name : apiDetails()");
		}
		return apiModel;
	}
	public APIModel getApiDetails(String apiKey) {
		developerLog.debug("Entering into com.dci.rest.dao.APIDAO || Method Name : getApiDetails() ||");
		CallableStatement cs = null;
		ResultSet rs = null;
		Connection con = null;
		APIModel apiModel = null;
		try {
			if (con == null || con.isClosed())
				con = getConnection();
			System.out.println("{CALL SP8_GETOAUTH_CLIENT_DETAILS_ACCESS_TOKEN  (" +apiKey+")}");
			cs = con.prepareCall("{CALL SP8_GETOAUTH_CLIENT_DETAILS_ACCESS_TOKEN  (?)}");
			cs.setString(1, apiKey);
			rs = cs.executeQuery();
			while (rs != null && rs.next()) {
				apiModel = new APIModel();
				apiModel.setKeySecret(rs.getString("client_secret"));
				//	apiDetailList.add(apiModel);
			}
			return apiModel;
		} catch (Exception e) {
			new DocubuilderException("Exception in com.dci.rest.dao.APIDAO || getApiDetails()", e);
		} finally {
			releaseConStmts(rs, cs, con, null);
			developerLog.debug("Exiting from com.dci.rest.dao.APIDAO || Method Name : getApiDetails()");
		}
		return apiModel;
	}
}
