/*
 * DocuBuilder
 * Copyright (c) 2005, 2004 Data Communique International, Inc.
 * All Rights Reserved.
 * 
 * This code is Intellectual Property ("IP") of 
 * Data Communique International, Inc. ("DCI").
 *  
 * Any form of distribution or redistribution to any 
 * party other than DCI is not permitted unless authorized 
 * by DCI in writing.
 *  
 * All users (programmers, consultanst or vendors appointed 
 * by DCI) of this software must return to
 * 		Data Comunique International
 * 		330 Washington Ave
 * 		Carlstadt, NJ - 07072
 * any improvements or extensions that they make 
 * and grant the IP rights to DCI.
 * 
 */
package com.dci.rest.dao;
import java.sql.*;
import java.util.*;

import javax.naming.*;
//import java.net.*;
//import org.apache.log4j.Logger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.dci.rest.exception.DocubuilderException;
/**
 * @author Anil Kumar
 *
 */


@Component
@Repository
public class DataAccessObject implements com.dci.rest.config.DocuBuilder {

	/**
	 * @purpose:Get the database connection.
	 * @return Connection
	 */
	@Autowired
	@Qualifier("firstdatasource")
	private DataSource firstDataSource;
	
	@Autowired
	@Qualifier("seconddatasource")
	private DataSource secondDataSource;
	
	
private static Logger developerLog = LogManager.getLogger(DataAccessObject.class);

/*
 @SuppressWarnings("rawtypes")
public synchronized static Connection getConnection(){
		InitialContext ctx = null;
		Connection conn = null;
		Hashtable parms = new Hashtable();
		try {
			javax.sql.DataSource ds = null;
			ctx = new InitialContext(parms);
			ds = (javax.sql.DataSource) ctx.lookup(DATASOURCE);
	        conn = ds.getConnection(USERID,PASSWD);
			
	        System.out.println("conn.getCatalog():"+conn.getCatalog());
	    } catch (Exception e) {
			new DocubuilderException("Exception in om.dci.rest.dao.DataAccessObject || getConnection()",e);
		}
		finally{
			//developerLog.debug("Exiting from com.dci.webservice.master.dao.DataAccessObject || Method Name : getConnection() ||");
		}
	 	return conn;
	 	
	}
	*/
/**
 * @purpose:Get the database connection.
 * @return Connection
 */
			public Connection getConnection()
			{
				Connection conn=null;
				try {
					conn=firstDataSource.getConnection();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return conn;
				
				
			}
 @SuppressWarnings("rawtypes")
public synchronized static Connection getConnectionRTC(){
		InitialContext ctx = null;
		Connection conn = null;
		Hashtable parms = new Hashtable();
		try {
			javax.sql.DataSource ds = null;
			ctx = new InitialContext(parms);
			ds = (javax.sql.DataSource) ctx.lookup(DATASOURCE);
	        conn = ds.getConnection(USERID,PASSWD);
	    } catch (Exception e) {
			new DocubuilderException("Exception in om.dci.rest.dao.DataAccessObject || getConnection()",e);
		}
		finally{
			//developerLog.debug("Exiting from com.dci.webservice.master.dao.DataAccessObject || Method Name : getConnection() ||");
		}
	 	return conn;
	 	
	}
 /**
	 * @purpose:Get the database connection.
	 * @return Connection
	 */
	 /*public static Connection getXBRLConnection(){
		 	MDC.put("category","com.dci.webservice.master.dao.DataAccessObject");
			developerLog.debug("Entering into com.dci.webservice.master.dao.DataAccessObject || Method Name : getXBRLConnection() ||");
			InitialContext ctx = null;
			Connection conn = null;
			Hashtable parms = new Hashtable();
			try {
			//	parms.put(
				//Context.INITIAL_CONTEXT_FACTORY, 
				//"com.ibm.websphere.naming.WsnInitialContextFactory"); 
				javax.sql.DataSource ds = null;
				ctx = new InitialContext(parms);
				ds = (javax.sql.DataSource) ctx.lookup(XBRLDATASOURCE);
				conn = ds.getConnection(USERID,PASSWD);
			} catch (Exception e) {
				new DCIException("getXBRLConnection()  || Java File Name: DataAccessObject ||",e);			
			}
			finally{
				developerLog.debug("Exiting from com.dci.webservice.master.dao.DataAccessObject || Method Name : getXBRLConnection() ||");
			}
			return conn;
		}*/
 
 
	/**
	 * @purpose:Get the database auth connection.
	 * @return Connection
	 */
 /*
	@SuppressWarnings("rawtypes")
	public static Connection getAuthConnection() {
		InitialContext ctx = null;
		Connection conn = null;
		Hashtable parms = new Hashtable();
		try {
			javax.sql.DataSource ds = null;
			ctx = new InitialContext(parms);
			ds = (javax.sql.DataSource) ctx.lookup(AUTHDATASOURCE);
			conn = ds.getConnection(USERID, PASSWD);
		} catch (Exception e) {
			new DocubuilderException("Exception in om.dci.rest.dao.DataAccessObject || getAuthConnection()",e);
		} finally {
			//developerLog.debug("Exiting from com.dci.webservice.master.dao.DataAccessObject || Method Name : getAuthConnection() ||");
		}
		return conn;
	}
	
	*/
	 public Connection getAuthConnection()
	 {  Connection conn=null;
		 try {
			conn=secondDataSource.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return conn;
	 }
	 /**
	 /**
	  * @purpose: release connection
	  * @return void
	  * */
	 public static void releaseConStmts(ResultSet rs,CallableStatement cs,Connection con,String expTrace){
		try {
			if (rs != null)
				rs.close();
			if (cs != null)
				cs.close();
			if (con != null)
				con.close();
		} catch (Exception ee) {
			developerLog.error("com.dci.rest.dao.DataAccessObject || Method Name : releaseConStmts() :"+ee,ee);
			//new DCIException(expTrace,ee);
		}
	}
	 
	
	 
	 
}
