/*
 * DocuBuilder
 * Copyright (c) 2005, 2004 Data Communique International, Inc.
 * All Rights Reserved.
 * 
 * This code is Intellectual Property ("IP") of 
 * Data Communique International, Inc. ("DCI").
 *  
 * Any form of distribution or redistribution to any 
 * party other than DCI is not permitted unless authorized 
 * by DCI in writing.
 *  
 * All users (programmers, consultanst or vendors appointed 
 * by DCI) of this software must return to
 * 		Data Comunique International
 * 		330 Washington Ave
 * 		Carlstadt, NJ - 07072
 * any improvements or extensions that they make 
 * and grant the IP rights to DCI.
 * 
 */
	package com.dci.rest.config;

/**
 * @author Prasad Aware
 *
 */
public interface DocuBuilder {
	
 public static final String SEPERATOR = "*";	
 
/**
 *  Session Variables
 */

 public static final String USEREMAIL = "userId";	
 public static final String USER = "user";	
 public static final String CLIENT = "client";
 public static final String DOCUMENT = "opendoc";
 public static final String CLIENTINFO = "clientinfo";
 public static final String RESOURCEBUNDLE = "resourcebundle";
 public static final String ADMINRESOURCEBUNDLE = "adminresourcebundle";
 public static final String LDAPRESOURCEBUNDLE = "ldapresourcebundle";
 public static final String FILESYSTEM_PATH = "/docubuilder/graph/";  // datacom is sharename
 public static final String QUANTUPLOAD_PATH = "/docubuilder/upload/";  // datacom is sharename
 public static final String BLANKPDF_PATH = "/docubuilder/graph/blank.pdf";
 public static final String TEMP_PATH = "r:";
 public static final String SHAREPATH = "/datacom";  
 public static final String BOOK_TITLE = "bookTitle";
/**  
 * Database Info	
 */
 //public static final String USERID = "AITUSER";
 //public static final String PASSWD = "aitus3r"; 
 public static final String USERID = "DCIAPPUSER";
 public static final String PASSWD = "DCIUSER"; 
//	public static final String USERID = "SPUNUKOLLU";
//	public static final String PASSWD = "PUNUKOLLUS";
 public static final String DATASOURCE = "jdbc/docubuildrp";
 public static final String DATASOURCE_ = "jdbc/docubuildrtc";
 public static final String XBRLDATASOURCE = "jdbc/docubuildxbrl";
 public static final String AUTHDATASOURCE = "jdbc/docubuildauth";
 
 public static final String LIBRARY = "docubuildx";
 public static final String CONTENTCATEGORY = "VEHICLEASSETCLASS";
 public static final String SELECTPERIOD = "selectperiod";
 public static final String BOOKTYPELIST = "booktypelist";
 
}
