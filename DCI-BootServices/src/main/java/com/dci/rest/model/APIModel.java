package com.dci.rest.model;

import org.springframework.context.annotation.Scope;
import org.springframework.security.core.token.Token;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class APIModel {
	
	private String id;
	private String userName;
	private String name;
	private String description;
	private String access_token;
	private String base_URL;
	private String schemaSelector;
	
	
	private String status;

	private String createdBy;
	private String updatedBy;
	
	private String app_URL;

 	private String callback_URL;
 	private String auth_URL;
 	
 	private String request_token;
 	private String secrete_token;
 
 	private String consumer_Key;
 	private String verification_Key;
 	private String public_Key;
 	private String private_Key;
	private String apiType;
	private String keyId;
	private String keySecret;
	
	private Token token = null;


 	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public String getSchemaSelector() {
		return schemaSelector;
	}

	public void setSchemaSelector(String schemaSelector) {
		this.schemaSelector = schemaSelector;
	}

	public String getApp_URL() {
		return app_URL;
	}

	public void setApp_URL(String app_URL) {
		this.app_URL = app_URL;
	}

	public String getCallback_URL() {
		return callback_URL;
	}

	public void setCallback_URL(String callback_URL) {
		this.callback_URL = callback_URL;
	}

	public String getAuth_URL() {
		return auth_URL;
	}

	public void setAuth_URL(String auth_URL) {
		this.auth_URL = auth_URL;
	}

	public String getRequest_token() {
		return request_token;
	}

	public void setRequest_token(String request_token) {
		this.request_token = request_token;
	}

	public String getSecrete_token() {
		return secrete_token;
	}

	public void setSecrete_token(String secrete_token) {
		this.secrete_token = secrete_token;
	}

	public String getConsumer_Key() {
		return consumer_Key;
	}

	public void setConsumer_Key(String consumer_Key) {
		this.consumer_Key = consumer_Key;
	}

	public String getVerification_Key() {
		return verification_Key;
	}

	public void setVerification_Key(String verification_Key) {
		this.verification_Key = verification_Key;
	}

	public String getPublic_Key() {
		return public_Key;
	}

	public void setPublic_Key(String public_Key) {
		this.public_Key = public_Key;
	}

	public String getPrivate_Key() {
		return private_Key;
	}

	public void setPrivate_Key(String private_Key) {
		this.private_Key = private_Key;
	}

	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}

	public String getBase_URL() {
		return base_URL;
	}

	public void setBase_URL(String base_URL) {
		this.base_URL = base_URL;
	}

	public String getApiType() {
		return apiType;
	}

	public void setApiType(String apiType) {
		this.apiType = apiType;
	}

/*	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}*/

	public String getKeyId() {
		return keyId;
	}

	public void setKeyId(String keyId) {
		this.keyId = keyId;
	}

	public String getKeySecret() {
		return keySecret;
	}

	public void setKeySecret(String keySecret) {
		this.keySecret = keySecret;
	}


}
