package com.dci.rest.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dci.rest.utils.StringUtility;
import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ComponentBean implements DciValidation{
	private String operation;
	
	@NotNull(groups = {ValidationCreateComp.class})
	private String type;
	
	@NotNull(groups = {ValidationAppasese.class})
	private String body;
	@NotNull(groups = {ValidationCreateComp.class,ValidationCreateLinkageComponent.class})
	private String name;
	
	private String effectiveDate;
	private String expirationDate;
	private String createdBy;
	private String lastUpdatedBy;
	
	@NotNull(groups = {ValidationCreateComp.class,ValidationCreateLinkageComponent.class})
	private String locale;
	
	private String context;
	private String convertTo;
	
	@NotNull(groups = {ValidationDeactivateComp.class})
	private String elementInstanceId;
	private String createdTime;
	private String lastUpdatedTime;
	
	private String global;
	private String qualDataId;
	private String qualData_Id;
	private String qualDataCon;
	private String qualDataTableType;
	private String quantDataId;
	@NotNull(groups = {ValidationCreateComment.class})
	private String comment;
	private String commentCount;
	
	private String entityIds;
	private String systemEntityIds;
	private String recursiveIds;
	private String footnoteIds;
	private String oldEntityIds;
	private String oldSystemEntityIds;
	private String oldRecursiveIds;
	private String oldFootnoteIds;
	
	private String footnoteMapping;
	private String previousFootnoteMapping;
	
	private String bookInstanceId;
	private String bookDetailId;
	private String documentTypeAssociation;
	private String assetClassAssociation;
	private String fundsAssociation;
	private String documentAssociation;
	private String associationType;
	private String primaryFlag;
	private String owner;
	private String fElementId;
	private String fQualDataDisc;
	private String days;
	
	/*Added for table - start*/
	private String rows;
	private String columns;
	private String tableType;
	private String dataTypeInd;
	private String mergeCellInfo;
	private String tableBody;
	private String rowStyles;
	/*Added for table - end*/
	private String localeDesc;
	
	@NotNull(groups = {ValidationGetFundByAsset.class})
	private String assetClass;
	private String attachment;
	private String FEffective_Ind;	
	private String fMarkerId;
	@Autowired
	List<Order> orderList;
	@Autowired
	List<Placement> placementList;
	@Autowired
	List<Style> styleList;
	

	private String fcomp_assignee;
	
	//for pending work
	private String fundsAssociationId;
	private String contextId;
	public String getParentContextId() {
		return parentContextId;
	}

	public void setParentContextId(String parentContextId) {
		this.parentContextId = parentContextId;
	}
	private String parentContextId;
	private String variableId;
	private String variableName;
	private String variableValue;
	
	
	
	public String getfElementId() {
		return fElementId;
	}

	public void setfElementId(String fElementId) {
		this.fElementId = fElementId;
	}

	public String getfQualDataDisc() {
		return fQualDataDisc;
	}

	public void setfQualDataDisc(String fQualDataDisc) {
		this.fQualDataDisc = fQualDataDisc;
	}

	public String getDays() {
		return days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public String getTableType() {
		return tableType;
	}

	public void setTableType(String tableType) {
		this.tableType = tableType;
	}

	public String getLocaleDesc() {
		return localeDesc;
	}

	public void setLocaleDesc(String localeDesc) {
		this.localeDesc = localeDesc;
	}
	@NotNull(groups = {ValidateStatusId.class})
	private String statusId; //to set the status id of the element for locale enhancement
	private String statusDesc; //to set the status description of the element for locale enhancement
	
	private boolean isParentPrimary;
	private String primaryIndicator;
	private String primaryComponentId;
	@NotNull(groups = {ValidationSetTag.class})
	private String tags;
	private String version;
	private String shadowId;
	
	private String overrideStatus;
	
	private String localeInd = null;// get language indicator of the component
	private String addmore="";
	private String fHeaderFooterInd = null;	
	private UserBean user;
	private String confidenceScore;
	//added for appasesne service instead of elementinstance id
	private String key;
	private boolean noDiff = false;
	@Autowired
	FootNoteAttributes footNoteAttributes;
	
	private String workFlowStatus;
	private String workFlowStatusId;
	
	private String fund[];
	private long unixtimestamplastUpdatedTime;
	public boolean isNoDiff() {
		return noDiff;
	}

	public void setNoDiff(boolean noDiff) {
		this.noDiff = noDiff;
	}

	public String getConfidenceScore() {
		return confidenceScore;
	}

	public void setConfidenceScore(String confidenceScore) {
		this.confidenceScore = confidenceScore;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	@Autowired
	List<Tag> tagAssociation;
	
	public String getElementInstanceId() {
		return elementInstanceId;
	}

	public void setElementInstanceId(String elementInstanceId) {
		this.elementInstanceId = elementInstanceId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = StringUtility.replaceControlCharacters(body,null);;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = StringUtility.replaceControlCharacters(name,null);
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	/*public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}*/

	public String getConvertTo() {
		return convertTo;
	}

	public void setConvertTo(String convertTo) {
		this.convertTo = convertTo;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getDocumentTypeAssociation() {
		return documentTypeAssociation;
	}

	public void setDocumentTypeAssociation(String documentTypeAssociation) {
		this.documentTypeAssociation = documentTypeAssociation;
	}
	
	public String getGlobal() {
		return global;
	}

	public void setGlobal(String global) {
		this.global = global;
	}

	public String getQualDataId() {
		return qualDataId;
	}

	public void setQualDataId(String qualDataId) {
		this.qualDataId = qualDataId;
	}

	public String getQualDataCon() {
		return qualDataCon;
	}

	public void setQualDataCon(String qualDataCon) {
		this.qualDataCon = qualDataCon;
	}

	public String getQualDataTableType() {
		return qualDataTableType;
	}

	public void setQualDataTableType(String qualDataTableType) {
		this.qualDataTableType = qualDataTableType;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getEntityIds() {
		return entityIds;
	}

	public void setEntityIds(String entityIds) {
		this.entityIds = entityIds;
	}

	public String getSystemEntityIds() {
		return systemEntityIds;
	}

	public void setSystemEntityIds(String systemEntityIds) {
		this.systemEntityIds = systemEntityIds;
	}

	public String getRecursiveIds() {
		return recursiveIds;
	}

	public void setRecursiveIds(String recursiveIds) {
		this.recursiveIds = recursiveIds;
	}

	public String getFootnoteIds() {
		return footnoteIds;
	}

	public void setFootnoteIds(String footnoteIds) {
		this.footnoteIds = footnoteIds;
	}

	public String getFootnoteMapping() {
		return footnoteMapping;
	}

	public void setFootnoteMapping(String footnoteMapping) {
		this.footnoteMapping = footnoteMapping;
	}

	public String getPreviousFootnoteMapping() {
		return previousFootnoteMapping;
	}

	public void setPreviousFootnoteMapping(String previousFootnoteMapping) {
		this.previousFootnoteMapping = previousFootnoteMapping;
	}

	public String getAssetClassAssociation() {
		return assetClassAssociation;
	}

	public void setAssetClassAssociation(String assetClassAssociation) {
		this.assetClassAssociation = assetClassAssociation;
	}

	public String getFundsAssociation() {
		return fundsAssociation;
	}

	public void setFundsAssociation(String fundsAssociation) {
		this.fundsAssociation = fundsAssociation;
	}

	/*public List<DocumentInfo> getDocumentAssociation() {
		return documentAssociation;
	}

	public void setDocumentAssociation(List<DocumentInfo> documentAssociation) {
		this.documentAssociation = documentAssociation;
	}*/

	public String getAssociationType() {
		return associationType;
	}

	public void setAssociationType(String associationType) {
		this.associationType = associationType;
	}

	public String getPrimaryFlag() {
		return primaryFlag;
	}

	public void setPrimaryFlag(String primaryFlag) {
		this.primaryFlag = primaryFlag;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public String getStatusDesc() {
		return statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public boolean isParentPrimary() {
		return isParentPrimary;
	}

	public void setParentPrimary(boolean isParentPrimary) {
		this.isParentPrimary = isParentPrimary;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getPrimaryIndicator() {
		return primaryIndicator;
	}

	public void setPrimaryIndicator(String primaryIndicator) {
		this.primaryIndicator = primaryIndicator;
	}

	public String getPrimaryComponentId() {
		return primaryComponentId;
	}

	public void setPrimaryComponentId(String primaryComponentId) {
		this.primaryComponentId = primaryComponentId;
	}

	public String getQualData_Id() {
		return qualData_Id;
	}

	public void setQualData_Id(String qualData_Id) {
		this.qualData_Id = qualData_Id;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(String lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public String getQuantDataId() {
		return quantDataId;
	}

	public void setQuantDataId(String quantDataId) {
		this.quantDataId = quantDataId;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getOldEntityIds() {
		return oldEntityIds;
	}

	public void setOldEntityIds(String oldEntityIds) {
		this.oldEntityIds = oldEntityIds;
	}

	public String getOldSystemEntityIds() {
		return oldSystemEntityIds;
	}

	public void setOldSystemEntityIds(String oldSystemEntityIds) {
		this.oldSystemEntityIds = oldSystemEntityIds;
	}

	public String getOldRecursiveIds() {
		return oldRecursiveIds;
	}

	public void setOldRecursiveIds(String oldRecursiveIds) {
		this.oldRecursiveIds = oldRecursiveIds;
	}

	public String getOldFootnoteIds() {
		return oldFootnoteIds;
	}

	public void setOldFootnoteIds(String oldFootnoteIds) {
		this.oldFootnoteIds = oldFootnoteIds;
	}

	public String getDocumentAssociation() {
		return documentAssociation;
	}

	public void setDocumentAssociation(String documentAssociation) {
		this.documentAssociation = documentAssociation;
	}

	public String getOverrideStatus() {
		return overrideStatus;
	}

	public void setOverrideStatus(String overrideStatus) {
		this.overrideStatus = overrideStatus;
	}

	public String getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(String commentCount) {
		this.commentCount = commentCount;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getShadowId() {
		return shadowId;
	}

	public void setShadowId(String shadowId) {
		this.shadowId = shadowId;
	}	

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public final String getLocaleInd() {
		return localeInd;
	}

	public final void setLocaleInd(String localeInd) {
		this.localeInd = localeInd;
	}

	public final String getAddmore() {
		return addmore;
	}

	public final void setAddmore(String addmore) {
		this.addmore = addmore;
	}

	public final String getfHeaderFooterInd() {
		return fHeaderFooterInd;
	}

	public final void setfHeaderFooterInd(String fHeaderFooterInd) {
		this.fHeaderFooterInd = fHeaderFooterInd;
	}

	public final UserBean getUser() {
		return user;
	}

	public final void setUser(UserBean user) {
		this.user = user;
	}

	public List<Tag> getTagAssociation() {
		return tagAssociation;
	}

	public void setTagAssociation(List<Tag> tagAssociation) {
		this.tagAssociation = tagAssociation;
	}

	public String getBookInstanceId() {
		return bookInstanceId;
	}

	public void setBookInstanceId(String bookInstanceId) {
		this.bookInstanceId = bookInstanceId;
	}

	public String getBookDetailId() {
		return bookDetailId;
	}

	public void setBookDetailId(String bookDetailId) {
		this.bookDetailId = bookDetailId;
	}

	public String getMergeCellInfo() {
		return mergeCellInfo;
	}

	public void setMergeCellInfo(String mergeCellInfo) {
		this.mergeCellInfo = mergeCellInfo;
	}

	public String getTableBody() {
		return tableBody;
	}

	public void setTableBody(String tableBody) {
		this.tableBody = tableBody;
	}

	public String getRowStyles() {
		return rowStyles;
	}

	public void setRowStyles(String rowStyles) {
		this.rowStyles = rowStyles;
	}

	public String getRows() {
		return rows;
	}

	public void setRows(String rows) {
		this.rows = rows;
	}

	public String getColumns() {
		return columns;
	}

	public void setColumns(String columns) {
		this.columns = columns;
	}

	public String getDataTypeInd() {
		return dataTypeInd;
	}

	public void setDataTypeInd(String dataTypeInd) {
		this.dataTypeInd = dataTypeInd;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getFEffective_Ind() {
		return FEffective_Ind;
	}

	public void setFEffective_Ind(String fEffective_Ind) {
		FEffective_Ind = fEffective_Ind;
	}

	public String getfMarkerId() {
		return fMarkerId;
	}

	public void setfMarkerId(String fMarkerId) {
		this.fMarkerId = fMarkerId;
	}

	public FootNoteAttributes getFootNoteAttributes() {
		return footNoteAttributes;
	}

	public void setFootNoteAttributes(FootNoteAttributes footNoteAttributes) {
		this.footNoteAttributes = footNoteAttributes;
	}

	public List<Order> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<Order> orderList) {
		this.orderList = orderList;
	}

	public List<Placement> getPlacementList() {
		return placementList;
	}

	public void setPlacementList(List<Placement> placementList) {
		this.placementList = placementList;
	}

	public List<Style> getStyleList() {
		return styleList;
	}

	public void setStyleList(List<Style> styleList) {
		this.styleList = styleList;
	}

	public String getAssetClass() {
		return assetClass;
	}

	public void setAssetClass(String assetClass) {
		this.assetClass = assetClass;
	}

	public String getWorkFlowStatus() {
		return workFlowStatus;
	}

	public void setWorkFlowStatus(String workFlowStatus) {
		this.workFlowStatus = workFlowStatus;
	}

	public String getWorkFlowStatusId() {
		return workFlowStatusId;
	}

	public void setWorkFlowStatusId(String workFlowStatusId) {
		this.workFlowStatusId = workFlowStatusId;
	}

	public String getFcomp_assignee() {
		return fcomp_assignee;
	}

	public void setFcomp_assignee(String fcomp_assignee) {
		this.fcomp_assignee = fcomp_assignee;
	}

	public String[] getFund() {
		return fund;
	}

	public void setFund(String[] fund) {
		this.fund = fund;
	}

	public long getUnixtimestamplastUpdatedTime() {
		return unixtimestamplastUpdatedTime;
	}

	public void setUnixtimestamplastUpdatedTime(long unixtimestamplastUpdatedTime) {
		this.unixtimestamplastUpdatedTime = unixtimestamplastUpdatedTime;
	}

	public String getFundsAssociationId() {
		return fundsAssociationId;
	}

	public void setFundsAssociationId(String fundsAssociationId) {
		this.fundsAssociationId = fundsAssociationId;
	}

	public String getVariableId() {
		return variableId;
	}

	public void setVariableId(String variableId) {
		this.variableId = variableId;
	}

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public String getVariableValue() {
		return variableValue;
	}

	public void setVariableValue(String variableValue) {
		this.variableValue = variableValue;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}
    
	

}
