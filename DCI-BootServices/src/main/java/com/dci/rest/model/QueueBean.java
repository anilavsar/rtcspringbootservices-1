package com.dci.rest.model;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class QueueBean {
	private int index;
	private int id;
	private String name;
	private String type;
	private String statusDesc;
	private int status;
	private String proofStatusDesc;
	private String proofStatus;
	private String creator;
	private String effDate;
	private String expDate;
	private String startTime;
	private String endTime;
	private String documentName;
	private int bookDetailId;
	private int bookInstanceID;
	private String fileName;
	private int parentId;
	private String section;
	private String errorDec;
	
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getProofStatusDesc() {
		return proofStatusDesc;
	}
	public void setProofStatusDesc(String proofStatusDesc) {
		this.proofStatusDesc = proofStatusDesc;
	}
	public String getProofStatus() {
		return proofStatus;
	}
	public void setProofStatus(String proofStatus) {
		this.proofStatus = proofStatus;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}	
	public String getEffDate() {
		return effDate;
	}
	public void setEffDate(String effDate) {
		this.effDate = effDate;
	}
	public String getExpDate() {
		return expDate;
	}
	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public int getBookDetailId() {
		return bookDetailId;
	}
	public void setBookDetailId(int bookDetailId) {
		this.bookDetailId = bookDetailId;
	}
	public int getBookInstanceID() {
		return bookInstanceID;
	}
	public void setBookInstanceID(int bookInstanceID) {
		this.bookInstanceID = bookInstanceID;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getErrorDec() {
		return errorDec;
	}
	public void setErrorDec(String errorDec) {
		this.errorDec = errorDec;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	
}
