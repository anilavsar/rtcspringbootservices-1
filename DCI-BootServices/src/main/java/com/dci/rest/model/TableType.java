package com.dci.rest.model;

public class TableType {
	private int id;
	private String internalName;
	private String name;
	private String flexiColInd;
	private String headerFooterInd;
	private String docType;
	private String displayName;
	
	public String getDocType() {
		return docType;
	}
	public void setDocType(String docType) {
		this.docType = docType;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getInternalName() {
		return internalName;
	}
	public void setInternalName(String internalName) {
		this.internalName = internalName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFlexiColInd() {
		return flexiColInd;
	}
	public void setFlexiColInd(String flexiColInd) {
		this.flexiColInd = flexiColInd;
	}
	public String getHeaderFooterInd() {
		return headerFooterInd;
	}
	public void setHeaderFooterInd(String headerFooterInd) {
		this.headerFooterInd = headerFooterInd;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	
}
