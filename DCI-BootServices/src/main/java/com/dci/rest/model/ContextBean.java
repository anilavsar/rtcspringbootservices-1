package com.dci.rest.model;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.dci.rest.model.DciValidation.ValidationCreateComp;
import com.fasterxml.jackson.annotation.JsonInclude;

@Component
@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ContextBean implements DciValidation{
	private String schemaId;
	private int id;
	private int sOrder;
	private int order;
	private int pId;
	@NotNull(groups = {ValidationCreateContext.class})
	private String name;
	private List<ComponentStatus> subCatogory;
	private List<ContextBean> subCatList;
	private String sId;
	private String sname;
	private String ids;
	private String orderIds;
	private String groupId;
	private String contextCategory;
	private String groupIdAssociation;
	private String groupIdParentAssociation;
	private boolean permission = false;
	
	
	
	public List<ContextBean> getSubCatList() {
		return subCatList;
	}
	public void setSubCatList(List<ContextBean> subCatList) {
		this.subCatList = subCatList;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}

	public List<ComponentStatus> getSubCatogory() {
		return subCatogory;
	}
	public void setSubCatogory(List<ComponentStatus> subCatogory) {
		this.subCatogory = subCatogory;
	}
	public String getIds() {
		return ids;
	}
	public void setIds(String ids) {
		this.ids = ids;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public int getsOrder() {
		return sOrder;
	}
	public void setsOrder(int sOrder) {
		this.sOrder = sOrder;
	}
	public String getsId() {
		return sId;
	}
	public void setsId(String sId) {
		this.sId = sId;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public String getOrderIds() {
		return orderIds;
	}
	public void setOrderIds(String orderIds) {
		this.orderIds = orderIds;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getContextCategory() {
		return contextCategory;
	}
	public void setContextCategory(String contextCategory) {
		this.contextCategory = contextCategory;
	}
	public String getGroupIdAssociation() {
		return groupIdAssociation;
	}
	public void setGroupIdAssociation(String groupIdAssociation) {
		this.groupIdAssociation = groupIdAssociation;
	}
	public String getGroupIdParentAssociation() {
		return groupIdParentAssociation;
	}
	public void setGroupIdParentAssociation(String groupIdParentAssociation) {
		this.groupIdParentAssociation = groupIdParentAssociation;
	}
	public boolean isPermission() {
		return permission;
	}
	public void setPermission(boolean permission) {
		this.permission = permission;
	}
	public int getpId() {
		return pId;
	}
	public void setpId(int pId) {
		this.pId = pId;
	}
	public String getSchemaId() {
		return schemaId;
	}
	public void setSchemaId(String schemaId) {
		this.schemaId = schemaId;
	}
	
}
