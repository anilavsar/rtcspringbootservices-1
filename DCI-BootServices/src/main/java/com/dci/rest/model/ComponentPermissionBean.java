package com.dci.rest.model;

import java.util.ArrayList;

import org.springframework.context.annotation.Scope;

import com.fasterxml.jackson.annotation.JsonInclude;

@Scope("request")
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ComponentPermissionBean {
	private String type;
	private String internalName;
	private int permissionLevel;
	private ArrayList<String> permission = new ArrayList<String>();
	
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getPermissionLevel() {
		return permissionLevel;
	}
	public void setPermissionLevel(int permissionLevel) {
		this.permissionLevel = permissionLevel;
	}
	public void setPermission(ArrayList<String> permission) {
		this.permission = permission;
	}
	public ArrayList<String> getPermission() {
		return permission;
	}
	public String getInternalName() {
		return internalName;
	}
	public void setInternalName(String internalName) {
		this.internalName = internalName;
	}
}
