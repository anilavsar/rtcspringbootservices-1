package com.dci.rest.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import com.dci.rest.config.DocuBuilder;
import com.deltaxml.core.ComparatorInstantiationException;
import com.deltaxml.core.IdentityTransformerSetupException;
import com.deltaxml.core.ParserInstantiationException;
import com.deltaxml.core.PipelineProcessingException;
import com.deltaxml.core.PipelinedComparator;
import com.deltaxml.core.PipelinedComparatorException;
import com.deltaxml.core.TransformerInstantiationException;
import com.deltaxml.pipe.filters.NormalizeSpace;
import com.deltaxml.pipe.filters.WordByWordInfilter;
import com.deltaxml.pipe.filters.WordByWordOutfilter1;
import com.deltaxml.pipe.filters.WordByWordOutfilter2;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class DeltaXml {
	
	/**
	 * @Purpose: This function will call the difference functionality and creates the output file.
	 * @param file1 Path for Old version of Xml File
	 * @param file2 Path for New Version of Xml file
	 * @param xmlOutFilename Path for XML containing differences
	 * @return void
	 * @throws Exception
	 * TODO
	 */
	public static void doDiff(String oldXml, String newXml, String diffXml)
			throws FileNotFoundException, PipelinedComparatorException {
		String outfilter = DocuBuilder.SHAREPATH + "/docubuilder/stylesheets/Common/ignoreattributes.xsl";
		PipelinedComparator pc = new PipelinedComparator();
		pc.setComparatorFeature("http://deltaxml.com/api/feature/isFullDelta", true);
		pc.setInputFilters(new Class[] { NormalizeSpace.class, WordByWordInfilter.class });

		File[] fileList = new File[] { new File(outfilter) };
		Class[] classList = new Class[] { NormalizeSpace.class, WordByWordOutfilter1.class,
				WordByWordOutfilter2.class };
		List filtersList = new ArrayList();
		// add file filters
		for (int i = 0; i < fileList.length; i++)
			filtersList.add(fileList[i]);

		// add class filters
		for (int i = 0; i < classList.length; i++)
			filtersList.add(classList[i]);
		pc.setOutputFilters(filtersList);
		pc.compare(new File(oldXml), new File(newXml), new File(diffXml));
	}

	/**
	 * @Purpose: This function will call the difference functionality and returns the contents of the output file.
	 * @param file1 Path for Old version of Xml File
	 * @param file2 Path for New Version of Xml file
	 * @param xmlOutFilename Path for XML containing differences
	 * @return String
	 * @throws Exception
	 * TODO*/
	public static String doDiff(String oldXml, String newXml)
			throws FileNotFoundException, PipelinedComparatorException, PipelineProcessingException,
			IdentityTransformerSetupException, ComparatorInstantiationException, ParserInstantiationException,
			TransformerInstantiationException, Exception {
		String outfilter = DocuBuilder.SHAREPATH + "/docubuilder/stylesheets/Common/ignoreattributes.xsl";
		// developerLog.debug(outfilter);
		StringBuffer blackLinedXML = new StringBuffer();
		PipelinedComparator pc = new PipelinedComparator();
		pc.setComparatorFeature("http://deltaxml.com/api/feature/isFullDelta", true);
		pc.setInputFilters(new Class[] { NormalizeSpace.class, WordByWordInfilter.class });

		File[] fileList = new File[] { new File(outfilter) };
		Class[] classList = new Class[] { NormalizeSpace.class, WordByWordOutfilter1.class,
				WordByWordOutfilter2.class };
		List filtersList = new ArrayList();
		// add file filters
		for (int i = 0; i < fileList.length; i++)
			filtersList.add(fileList[i]);

		// add class filters
		for (int i = 0; i < classList.length; i++)
			filtersList.add(classList[i]);
		pc.setOutputFilters(filtersList);
		oldXml = CheckString.replaceString(oldXml, "&nbsp;", " ");
		newXml = CheckString.replaceString(newXml, "&nbsp;", " ");
		pc.compare(oldXml, newXml, blackLinedXML);
		return blackLinedXML.toString();
	}

	public static void main(String[] args) throws FileNotFoundException, PipelinedComparatorException {
	}
}